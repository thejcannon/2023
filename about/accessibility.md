---
title: Accessibility
---

{{ site.data.event.name }} welcomes attendees with disabilities and strives to
be inclusive and accessible.

## Hearing accessibility

PyGotham is pleased to provide CART (Communication Access Realtime
Transcription), otherwise known as live captioning, for all talks on all tracks.

Additionally, PyGotham is pleased to provide live American Sign Language
interpreters for all talks on all tracks.

## Vision accessibility

In most sessions within {{ site.data.event.name }}, material will be presented
via projection.

## Questions

If you have accessibility questions or require additional accommodations, please
email us at [organizers@pygotham.org](mailto:organizers@pygotham.org).

(Accessibility info template reused [from AdaCamp
toolkit](https://adacamp.org/adacamp-toolkit/website-content/#accessibility), CC
BY-SA.)
