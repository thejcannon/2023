---
name: Jürgen Gmach
talks:
- "Behind the Scenes of tox: The Journey of Rewriting a Python Tool with Over 10 Million Monthly Downloads"
---
I am a software developer with a passion for Python and Linux, developing
open source software both at my day job at
[Canonical](https://jugmac00.github.io/hiring/), and at night as a
maintainer of [tox](https://github.com/tox-dev/tox) and many other projects.

I would love to connect with you!

- [My blog](https://jugmac00.github.io/)
- [Mastodon](https://fosstodon.org/@jugmac00)
- [Twitter](https://twitter.com/jugmac00)
- [LinkedIn](https://www.linkedin.com/in/j%C3%BCrgen-gmach-b24363226/)
