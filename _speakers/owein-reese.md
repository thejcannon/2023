---
name: Owein Reese
talks:
- "Stop writing so many darn logs"
---
I was born, went to school, got a job and somewhere along the way I learned
to program. As a developer, I've worked on military systems, NASA satellite
programs, hedge funds, in an investment bank, in adTech, startups and now in
the predictive HR Arena. I've been both a tech lead and a manager, seen some
awful code and written a whole bunch myself. Most of my career has been
spent with functional programming, something I'm passionate about, and
Python.
