---
name: Jan Giacomelli
talks:
- "Celery on AWS ECS - the art of background tasks \u0026 continuous deployment"
---
I'm a Staff Software Engineer at [Ren](https://rensystems.com). I work with
Python for the last 7 years. I'm a TDD practitioner. I'm always searching
for better ways to deliver high-quality software at a rapid pace. I love
simplicity. I write for [testdriven.io](https://testdriven.io). When I'm not
programming, I'm windsurfing, skiing, playing squash, hiking, ...
