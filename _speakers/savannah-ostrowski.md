---
name: Savannah Ostrowski
talks:
- "Accelerate your workflow from local Python prototype to the cloud"
---
Savannah Ostrowski is a senior product manager at Microsoft, focused on
Azure cloud native developer tools and experience. She is the product lead
for the open source Azure Developer CLI (azd). Previously, she was the
product manager for the Pylance language server and focused on improving
Python developer experience in tools like Visual Studio Code, Visual Studio
and on Azure. She's passionate about Python, open source software, DevOps,
and developer experience.
