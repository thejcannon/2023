---
name: Brayan Kai Mwanyumba
talks:
- "Explaining Machine Learning Models with Interactive Techniques in Python"
---
Brayan Kai is a Data Scientist Passionate about Communities, Technical
Writing and Open-Source Advocacy. He currently volunteers at different
developer communities across Africa including Google Crowdsource, Open-
Source Community Africa, She Code Africa and Dev Careers. All this owing to
his strong passion for supporting fellow upcoming technologists, women in
tech and advocating for inclusion and diversity. He calls this his personal
mission. It makes him happier, more balanced, and gives him a stronger sense
of purpose to innovate, share, and teach in and with the community rather
than just for it.
