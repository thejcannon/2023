---
name: Bard Fetler
talks:
- "Are Large Language Models Inherently Biased?"
---
Bard is a machine learning engineer / data scientist with 20+ years
experience in software.  His current projects include medical entity
detection and studying LLMs.
