---
duration: 15
presentation_url:
room:
slot:
speakers:
- Petr Schreiber
- Tomáš Daněk
title: "Reverse engineering of video game data as alternative approach to exploration of Python universe"
type: talk
video_url:
---
Authors describe playful approach to exploration of powerful Python feature
set, by reverse engineering data file of a popular video game.

The approach rewards the explorer by giving immediate feedback and allows
the explorer to feel like a data archeologist at least for 1 afternoon.

The film takes form of a mini documentary, mixing parts of the gameplay with
a commented walk through of the code.
